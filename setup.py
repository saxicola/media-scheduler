#!/bin/env python3
'''
'''

from setuptools import setup, find_packages


with open("README.adoc", "r") as fh:
    long_description = fh.read()

setuptools.setup(

name="media-scheduler",
    version="0.0.0",
    description="",
    author="Mike Evans",
    author_email="mikee@saxicola.co.uk",
    license="MIT",
    scripts=['src/scheduler.py','src/messages.py', "src/media.py","version.py"],
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
    ],
    packages = find_packages(),
    package_dir={'medsched': 'src'},
    entry_points={"console_scripts": ["medsched=scheduler:main"]},
)
