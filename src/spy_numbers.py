#!/bin/env python3
# -*- coding: utf-8 -*-
'''
(C) 2010 Mike Evans <mikee@saxicola.co.uk>

A fake numbers station.
'''


import alsaaudio
import argparse
import logging
import os
import queue
import sys
import subprocess
#import ffmpeg
import sounddevice as sd



logging.basicConfig(level=logging.DEBUG,
        format='%(module)s: LINE %(lineno)d: %(levelname)s: %(asctime)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')


DEBUG = logging.debug
INFO = logging.info



def play_outro():
    return


def play_numbers():
    return

def play_intro():
    return


def main():
    try:
        message = sys.argv[1]
    except:
        print("This module needs a message parameter")
        quit(1)
    play_intro()

    return

if __name__ == '__main__':
    DEBUG("...and so it begins.")
    main()
