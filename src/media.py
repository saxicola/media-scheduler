# -*- coding: utf-8 -*-
'''
(C) 2010 Mike Evans <mikee@saxicola.co.uk>

Media with explicit dates are subservient to those with scheduled dates. So if repeat = 2 overide with
media with repeat = 0
'''

import calendar
from datetime import datetime, timedelta
from datetime import date
from dateutil import parser
import mplayer
import logging
import os
#import pulsectl
import requests
import re
import syslog
import time
# If we use sounddevice install with deps with pip3 install sounddevice ffmpeg-python
import numpy as np
import ffmpeg
import sounddevice as sd

'''
hostname = os.uname()[1]
if "wyse" in hostname: # Log to disk
    print("Logging to file")
    logging.basicConfig(filename='/home/mikee/scheduler.log', filemode='a', level=logging.DEBUG, format='%(module)s: LINE %(lineno)d: %(levelname)s: %(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
else:
    logging.basicConfig(level=logging.DEBUG, format='%(module)s: LINE %(lineno)d: %(levelname)s: %(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
'''
DEBUG = logging.debug
INFO = logging.info


class Media():
    self.vols = [0]*10 # This will a moving average of volumes.  # Size may need tweaking
    URL = None
    title = None
    start= None
    end = None
    repeat = None
    played = False
    repeat = 0 # 0 = No repeat, hourly = 1, daily = 2, weekly = 3, WTF = 4
    day_of_week = None # Which days of week to repeat.  Day 0 = Monday.
    #stop = False
    player = None
    special = False # Promote this item to play if this is a special, i.e., if it has a day or date

    def __init__(self, date_start, date_end , time_start, time_end, repeat, URL, title = None): # TODO Bug-323
        INFO("init medea.py")
        self.player = None # So we can access it globabally
        try: ## Sometimes pulse doesn't load, should this stop the player?
            self.pulse = pulsectl.Pulse('media.py')
        except:
            self.pulse = None
        date_start = date_start.strip()
        date_end = date_end.strip()
        time_start = time_start.strip()
        time_end = time_end.strip()
        self.repeat = 2 # Default is daily specifying a day of date overrides this TODO Bug-323
        self.URL = str(URL).strip().replace('"','')
        self.title = title.strip()
        DEBUG("{}, {}".format(date_start, date_end))
        # Test the start and end date for format
        # TODO: This is long and messy, refactor this and fix the repeat malarkey

        # Specials have a date or day name/number in the first field
        # Note to future me:
        # list(calendar.day_name) = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        # str.title() is title case the string.
        # Make the date and time strings into a timestamp
        start = None
        end = None
        if date_start.title() in list(calendar.day_name) or date_start.title() in list(calendar.day_abbr):
            # date_start is the name of a day soooo
            DEBUG(date_start.title())
            day_num = time.strptime(date_start[0:3], '%a').tm_wday
            date_start = self.get_next_date(day_num)
            self.repeat = 3
            self.special = True
            DEBUG(date_start)
        '''else: # Test if it's a date for some reason?
            try:
                datetime.strptime(date_start,"%Y-%m-%d")
                start = parser.parse(', '.join((date_start, time_start)))
                DEBUG(start)
            except ValueError as ve:# If this fails then this is not an ISO date string
                DEBUG(ve)
                pass
        '''
        start = parser.parse(', '.join((date_start, time_start)))
        end = parser.parse(', '.join((date_start, time_end)))
        # If end time spans midnight we have to add a day to the end date. But if start this up after
        # midnight and before the end of the midnight spanning event, we must not and subtract a day from the start time.
        now = datetime.now()
        d_time = end - start
        DEBUG(d_time.total_seconds())
        # Don't add a day to end if we are already after midnight
        now_sod = self.seconds_of_day(now)
        end_sod = self.seconds_of_day(end)
        start_sod = self.seconds_of_day(start)
        if start_sod > end_sod and now_sod < end_sod:
            DEBUG("Midnight spanning event")
            self.end = end
            self.start = start - timedelta(days = 1)
        elif d_time.total_seconds() < 0: # End time is after midnight but now isn't
            self.end = end + timedelta(days = 1) # So we add a day to the end time
            self.start = start
            INFO("Fixed a midnight spanning event.")
        else:
            self.end = end
            self.start = start
        assert(type(self.start) == datetime)
        assert(type(self.end) == datetime)
        DEBUG("{} to {}".format(self.start,self.end))


    def seconds_of_day(self, the_time):
        '''
        Calculate the the number of secons since the previous midnight of the supplied datetime
        @param datetime A datetime object
        '''
        assert(type(the_time) == datetime)
        return (the_time - the_time.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()


    def get_next_date(self, index):
        '''
        Given a day of the week number 0=Monday, return the date of the next occurrence relative to
        today
        '''
        today = date.today()
        nextday = today + timedelta( (index - today.weekday()) % 7 )
        return nextday.strftime("%Y-%m-%d")

    def get_streamtitle(self):
        return ''
        encoding = "latin1"
        info = ''
        url = self.URL
        radio_session = requests.Session()
        radio = radio_session.get(url, headers={'Icy-MetaData': '1'}, stream=True)
        metaint = int(radio.headers['icy-metaint'])
        stream = radio.raw
        audio_data = stream.read(metaint)
        meta_byte = stream.read(1)
        if (meta_byte):
            meta_length = ord(meta_byte) * 16
            meta_data = stream.read(meta_length).rstrip(b'\0')
            #print(meta_data)
            stream_title = re.search(br"StreamTitle='([^']*)';", meta_data)
            if stream_title:
                stream_title = stream_title.group(1).decode(encoding, errors='replace')
                if info != stream_title:
                    DEBUG('Now playing: {}'.format(stream_title))
                    return stream_title
                else:
                    pass
            else:
                return None
        return None # If we get here


    def test_play(self):
        player = mplayer.Player(args = ("-af equalizer=0:0:0:0:0:0:0:5:5:5,volnorm -softvol -volume 30") )
        url = "http://wprb.streamguys1.com/live"
        player.loadfile(url)
        time.sleep(10)
        #player.stop()
        quit()




    """
    def playstream_callback(self, outdata, frames, time, status):
        ''' This needs to manipulate output volume or there's no point
        to this.'''
        volume_norm = np.linalg.norm(outdata)
        # TODO get a rolling average to set the output volume
        vols.insert(0,volume_norm/frames) # Isert at the beginning
        vols.pop() # Pop off the end.
        mav = moving_average(vols, 10)
        print (int(volume_norm/frames), mav[0])
        # Call to amixer with subprocess to use this value
        # amixer set 'Master' volume%
        #p = subprocess("amixer", "set", "'Master'", "10%")

    try:
        data = q.get_nowait()
        except queue.Empty as e:
        print('Buffer is empty: increase buffersize?', file=sys.stderr)
        raise sd.CallbackAbort from e
        assert len(data) == len(outdata)
        outdata[:] = data
        return

    def play_stream(self):
        try:
            print('Opening stream ...')
            process = ffmpeg.input(
                url
            ).output(
                'pipe:',
                format='s16le',
                acodec='pcm_s16le',
                ac=channels,
                ar=samplerate,
                loglevel='quiet',
            ).run_async(pipe_stdout=True)
            stream = sd.RawOutputStream(
                samplerate=samplerate, blocksize=2048,
                device=args.device, channels=channels, dtype='int16',
                callback=callback)
            read_size = args.blocksize * channels * stream.samplesize
            print('Buffering ...')
            for _ in range(args.buffersize):
                q.put_nowait(process.stdout.read(read_size))
            print('Starting Playback ...')
            with stream:
                timeout = args.blocksize * args.buffersize / samplerate
                while True:
                    q.put(process.stdout.read(read_size), timeout=timeout)
        except KeyboardInterrupt:
            parser.exit('\nInterrupted by user')
        except queue.Full:
            # A timeout occurred, i.e. there was an error in the callback
            parser.exit(1)
        except Exception as e:
            parser.exit(type(e).__name__ + ': ' + str(e))

    """



    def play(self, volume = None):
        '''
        How do we stop a running item and play a new one?
        '''
        self.bluetooth_vol(0.4)
        url = self.URL.strip()
        DEBUG("Playing {}".format(url))
        if url == '':
            print("Nothing to play")
            DEBUG("Nothing to play")
            return
        if volume: player.volume = volume
        self.player = mplayer.Player(args = ("-loop 0 -af equalizer=0:0:0:0:0:0:0:5:5:5 -softvol -volume 30") )
        self.player.loadfile(url)
        while self.player.is_alive():
            time.sleep(1)
            if datetime.now() > self.end:
                self.played = True
                self.player.quit()
                DEBUG("Stopping {}".format(url))
                return
        # Update the next scheduled play
        if   self.played == 1 and self.repeat == 1: # Its an hourly repeat so add an hour
            self.start = self.start + timedelta(hours = 1)
            self.played = True # Reset the played back to unplayed
        elif self.played == 1 and self.repeat == 2: # Its a daily repeat so add a day
            self.start = self.start + timedelta(days = 1)
            self.played = True # Reset the played
        elif self.played == 1 and self.repeat == 3: # Its a weekly repeat so add a week
            self.start = self.start + timedelta(weeks = 1)
            self.played = True # Reset the played
        return


    def play_stop(self):
        DEBUG("Stopping {}".format(self.URL))
        if self.player:
            self.player.quit()
        DEBUG("Stopped {}".format(self.URL))


    def bluetooth_vol(self, vol):
        '''
        If we are outputting via bluetooth the volume is going to be too loud, probably
        so set it to a default, sensible, value.
        '''
        if self.pulse:
            for sink in self.pulse.sink_list():
                if "bluez_sink" in sink.name:
                    self.pulse.volume_set_all_chans(sink, vol)
            return True
        else:
            INFO("No pulse!")
            syslog.syslog("No pulse!  It may have crashed.")
            return False


    def to_csv(self):
        '''
        return a CSV formatted line suitable for writing to disk
        '''
        day_date = None
        if self.repeat == 2: day_date = ''
        elif self.repeat == 3: day_date = datetime.strftime(self.start, "%a")
        else: day_date = datetime.strftime(self.start, "%Y-%m-%d")
        return ','.join(( \
        day_date,
        '',
        datetime.strftime(self.start, "%H:%M"),
        datetime.strftime(self.end, "%H:%M"),
        str(self.repeat), # TODO Bug-323 , remove this
        self.URL,
        self.title))

    '''
    These setters have to rebuild the end date-tiem if either part changes
    '''
    def set_start_date(self, date_start):
        time_start = datetime.strftime(self.start, "%H:%M" )
        self.start = parser.parse(', '.join((date_start, time_start)))

    def set_end_date(self, date_end):
        time_end = datetime.strftime(self.end, "%H:%M" )
        self.end = parser.parse(', '.join((date_end, time_end)))

    def set_start_time(self, time_start):
        date_start = datetime.strftime(self.start,  "%Y-%m-%d" )
        self.start = parser.parse(', '.join((date_start, time_start)))

    def set_end_time(self, time_end):
        date_end = datetime.strftime(self.end,  "%Y-%m-%d" )
        self.end = parser.parse(', '.join((date_end, time_end)))



# Just play a test URL
if __name__ == "__main__":
    media = Media("","","09:00","12:00","2","foo","Radio 6 music")
    print(media.to_csv())
    #media.test()
