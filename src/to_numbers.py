#!/bin/env python3
# -*- coding: utf-8 -*-
'''
(C) 2010 Mike Evans <mikee@saxicola.co.uk>

Convert a message into a format suitable for converting to a numbers station.
Just use the ascii codes rather than those in
https://linuxcoffee.com/numbers/

'''


#import alsaaudio
import argparse
import gtts
import json
import logging
import os
from os.path import exists
import re
#from playsound import playsound
#import queue
import sys
import subprocess
import ffmpeg
#import sounddevice as sd


logging.basicConfig(level=logging.DEBUG,
        format='%(module)s: LINE %(lineno)d: %(levelname)s: %(asctime)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
DEBUG = logging.debug
INFO = logging.info


code = """
    A-1   K-77	U-84    (.)-91
    0-CODE	70-B	80-P	90-FIG
    B-70	L-78	V-85	(:)-92
    1-A		71-C	81-Q	91-(.)
    C-71	M-79	W-86	(')-93
    2-E		72-D	82-R	92-(:)
    D-72	N-4		X-87	()-94
    3-I		73-F	83-S	93-(')
    E-2		O-5		Y-88	(+)-95
    4-N		74-G	84-U	94-()
    F-73	P-80	Z-89	(-)-96
    5-O		75-H	85-V	95-(+)
    G-74	Q-81	FIG-90	(=)-97
    6-T		76-J	86-W	96-(-)
    H-75	R-82	SPC-99
    77-K	87-X	97-(=)
    I-3		S-83	CODE-0
    78-L	88-Y	98-REQ
    J-76	T-6		REQ-98
    79-M	89-Z	99-SPC
    """




class RevDict(dict):
    def __setitem__(self, key, value):
        # Remove any previous connections with these values
        if key in self:
            del self[key]
        if value in self:
            del self[value]
        dict.__setitem__(self, key, value)
        dict.__setitem__(self, value, key)

    def __delitem__(self, key):
        dict.__delitem__(self, self[key])
        dict.__delitem__(self, key)

    def __len__(self):
        """Returns the number of connections"""
        return dict.__len__(self) // 2


# Create our reversed dictionary from codes, which is copied from 'MANUAL ONE TIME PADS DIY-1.pdf' in doc
# Ideally this needs to happen once and saved to a file.
if not exists("ct37.json"):
    with open("ct37.json", "w") as f:
        codes = re.split(' |\t|\n',code)
        ct37 = {}
        for c in codes:
            if c != '':
                ct37[c.split('-')[0]] = c.split('-')[1]
        print(ct37)
        ct37c = RevDict()
        for key in ct37:
            ct37c[key] = ct37[key]
        #print(ct37c)
        json.dump(ct37c,f)
else:
    with open('ct37.json') as json_file:
        ct37 = json.load(json_file)
#DEBUG(ct37)




def encode(string):
    '''
    NOTE: This does not encrypt, call emcrypt(string)
    Convert 0 to 9 and a to Z to a number using CT-37c encoding.
    Using the CT37c table is easy. All characters are converted into their one-digit or two-digit value. To
    convert numbers, always use "FIG" before and after one ore more digits. Each digit is written out three
    times to exclude errors. You can use spaces and punctuations within the "FIG" mode. An example: "1.5
    KG" = "90 111 91 555 90 77 74". The "REQ" or "REQUEST" field enables questions and spaces are
    created with the "SPC" field. The apostrophe (93) can be used as both apostrophe and comma. The
    "CODE" field is the codebook prefix and is used before each codebook value. The use of spaces before
    and after codebook words is not necessary.

    A-1		K-77	U-84    (.)-91
    0-CODE	70-B	80-P	90-FIG
    B-70	L-78	V-85	(:)-92
    1-A		71-C	81-Q	91-(.)
    C-71	M-79	W-86	(')-93
    2-E		72-D	82-R	92-(:)
    D-72	N-4		X-87	()-94
    3-I		73-F	83-S	93-(')
    E-2		O-5		Y-88	(+)-95
    4-N		74-G	84-U	94-()
    F-73	P-80	Z-89	(-)-96
    5-O		75-H	85-V	95-(+)
    G-74	Q-81	FIG-90	(=)-97
    6-T		76-J	86-W	96-(-)
    H-75	R-82	SPC-99
    77-K	87-X	97-(=)
    I-3		S-83	CODE-0
    78-L	88-Y	98-REQ
    J-76	T-6		REQ-98
    79-M	89-Z	99-SPC

    Optional short codes:

    000	    ABORT		253		DECODE		505		MILITARY	758		STREET
    019		ACCEPT		262		DELAY		514		MONEY		767		SUBWAY
    028		ACCESS		271		DIFFICULT	523		MONTH		776		SUCCESS
    037		ADDRESS		280		DOCUMENT	532		MORNING		785		SUPPLY
    046		AFFIRMATIVE	299		ENCODE		541		MORSE		794		SUPPORT
    055		AGENT		307		EVENING		550		NEGATIVE	802		TELEPHONE
    064		AIRPLANE	316		EXECUTE		569		NIGHT		811		TODAY
    073		AIRPORT		325		FACTORY		578		OBSERVATION	820		TOMORROW
    082		ANSWER		334		FAILED		587		PASSPORT	839		TRAIN
    091		AUTHORITY	343		FERRY		596		PERSON		848		TRANSFER
    109		BETWEEN		352		FLIGHT		604		PHOTOGRAPH	857		TRANSMIT
    118		BORDER		361		FREQUENCY	613		POSITIVE	866		TRAVEL
    127		BUILDING	370		HARBOUR		622		POSSIBLE	875		TRUCK
    136		CANCEL		389		HELICOPTER	631		POWER		884		UNABLE	TO
    145		CHANGE		398		HIGHWAY		640		PRIORITY	893		URGENT
    154		CIVILIAN	406		IDENTITY	659		PROBLEM		901		VERIFY
    163		COMPROMISE	415		IMMEDIATE	668		QUESTION	910		WEEK
    172		COMPUTER	424		IMPOSSIBLE	677		RADIO		929		WITHIN
    181		CONFIRM		433		INFORMATION	686		RECEIVE		938		YESTERDAY
    190		CONTACT		442		INSTRUCTIONS695		RENDEZVOUS	947		DISREGARD MSG FROM
    208		COORDINATE	451		LOCATE		703		REPEAT		956		DO NOT ANSWER
    217		COUNTRY		460		LOCATION	712		RESERVATION	965		FORWARD THIS MSG TO
    226		COVERT		479		MAIL		721		ROUTINE		974		REPEAT YOUR MSG FROM
    235		CURRENT		488		MEETING		730		SATELLITE	983		READABILITY	(1 TO 5/5)
    244		DANGER		497		MESSAGE		749		SHIP		992		SIGNAL STRENGTH (1 TO 5)

    '''
    encoded = ''
    for c in string:
        if c != ' ':
            encoded += ct37[c.upper()]
    # Split into groups
    groups = [encoded[i:i+5] for i in range(0, len(encoded), 5)]
    string = ' '.join(groups)
    ng = []
    for group in groups:

        ng.append(','.join(group+('9' * (5 - len(group)))))
    encoded = '. '.join(ng) + '.'
    return encoded



    def encrypt(message):
        ''' TODO'''
        return



def main():
    try:
        message = sys.argv[1]
    except:
        print("This module needs a message parameter")
        quit(1)
    encoded = encode(message)
    DEBUG(encoded)
    tts = gtts.gTTS(encoded, slow=True)
    tts.save("message.mp3")
    return

if __name__ == '__main__':
    DEBUG("...and so it begins.")
    main()
