#!/bin/env python3
# -*- coding: utf-8 -*-
'''
Schedule streamed or local media to audio out.
Initial version 0 will work from a csv file of scheduled media, just t oget something going.
Then add a GUI for version 1

(C) 2010 Mike Evans <mikee@saxicola.co.uk>

'''


import sys, os
import logging
# Get python version
py_version = sys.version_info.major
import argparse
#import configparser
from datetime import datetime, date
import dateutil # for parser
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), os.path.pardir))
import copy
import csv

import locale
import gettext
import getpass
from media import Media
import messages
import mplayer
import os
import pulsectl
import signal
import subprocess
import syslog
import time
import threading

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk

_ = gettext.gettext

pwd = os.path.dirname(__file__)

try: import version
except:
    vf = open('version.py','w')
    vf.write("__version__ = \"devel\"\n")
    vf.close()
import version

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="Output INFO statements", action="store_true")
parser.add_argument("-i", "--info", help="Output INFO statements", action="store_true")
parser.add_argument("-d", "--debug", help = "Output DEBUG and INFO statements",  action="store_true")
parser.add_argument("-l", "--list", help = "Print the sorted play list", action = "store_true")
parser.add_argument("-g", "--gui", help=_("Run the experimental GUI version, if it exists."),  action="store_true")
parser.add_argument('schedfile', metavar='Schedule_file', type=str, nargs='+', help='The location of the schedule cvs file.')
parser.add_argument("-t", "--test", help=_("Play a test URL."),  action="store_true")

args = parser.parse_args()

hostname = os.uname()[1]
# Set up debugging output level for each host
if "wyse" in hostname: # Log to file
    print("Logging to file")
    logging.basicConfig(filename='/home/mikee/scheduler.log', filemode='a', level=logging.DEBUG,
            format='%(module)s: LINE %(lineno)d: %(levelname)s: %(asctime)s: %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')
else:
    logging.basicConfig(level=logging.DEBUG,
        format='%(module)s: LINE %(lineno)d: %(levelname)s: %(asctime)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')


DEBUG = logging.debug
INFO = logging.info
# Enable/disable debugging when requested
if args.info:
    logging.disable(logging.DEBUG)
if args.verbose:
    logging.disable(logging.DEBUG)
elif args.debug:
    pass
else:
    logging.disable(logging.DEBUG)
    logging.disable(logging.INFO)

DEBUG("Media scheduler start.")


class Schedule():
    '''
    Store the events and build a schedule
    '''
    def __init__(self):
        pass

######################### END Schedule ##############################


class Scheduler():
    def __init__(self):
        self.schedfile = args.schedfile[0] # save the file name for later.
        signal.signal(signal.SIGINT, self.exit_nicely)
        signal.signal(signal.SIGHUP, self.exit_nicely)

        #self.check_instance()
        self.playlist = [] # list of media to be played
        self.get_playlist()
        if args.list:
            self.print_schedule() ## This will quit after.
        #while(1): # Loop while time passes and schedules stuff.
        self.run_schedule(self.playlist)
        #    time.sleep(10) #
        #    #self.get_playlist()


    def check_instance(self):
        '''
        Check we are not already running and mail me.
        quit if already running.
        BOGUS: This iwill always report itself running.
        '''
        process = sys.argv[0]
        pid =  os.getpid()
        try: # This will fail if the process is not running
            print(subprocess.check_output(["pgrep", "-f","scheduler.py"]))
            print("Already running!  Quitting")
            # Mail me
            #quit()
        except: # No process found so carry on and don't panic Pike
            print("All good, carry on Pike!")
            return


    def decomment(self, csvfile):
        '''
        Remove lines that begin with a #
        '''
        for row in csvfile:
            raw = row.split('#')[0].strip()
            if raw: yield row


    def get_playlist(self):
        playdictlist = []
        try:
            scheds = open(args.schedfile[0], "r")
        except: return False
        fields = ["date_start", "date_end", "time_start", "time_end", "repeat", "URL", "description"] # TODO Bug-323 remove repeat
        csvreader = csv.DictReader(scheds, fieldnames = fields, delimiter=',', quotechar='"' )
        next(csvreader) # Skip header row
        # Build a play list
        for row in csvreader:
            if row['date_start'].strip().startswith('#'):
                continue
            ''' TODO: This is a start to making things simpler.
            itrow = {}
            itrow['date_start'] = row['date_start']
            itrow['date_end'] = row['date_end']
            itrow['time_start'] = row['time_start']
            itrow['time_end'] = row['time_end']
            itrow['repeat'] = row['repeat']
            itrow['URL'] = row['URL'].replace('"',''), row['description'].strip()
            itrow['description'] = row['description'].strip()
            #itrow['start'] = dateutil.parser.parse(', '.join((row['date_start'], row['time_start'])))
            #itrow['end'] = dateutil.parser.parse(', '.join((row['date_end'], row['time_end'])))
            '''
            try: # This is user input and lines may be malformed
                item = Media(row['date_start'], row['date_end'], row['time_start'], row['time_end'],row['repeat'], row['URL'].replace('"',''), row['description'].strip())
            except:
                raise
                INFO("A line in the CSV could not be read.")
                continue
            if item.URL != '':#if item:
                #playdictlist.append(itrow)
                DEBUG("{}, {}, {}, {}\n\n".format(item.start, item.end, datetime.now() <= item.start, item.URL))
                self.playlist.append(item)

        '''
        self.playlist.clear() # Empty it.
        DEBUG( self.playlist)
        for i in playdictlist:
            item = Media(i['date_start'], i['date_end'], i['time_start'],
            i['time_end'],i['repeat'],
            i['URL'], i['description'])
            self.playlist.append(item)
        '''

        # Sort it by date and priority
        #self.playlist.sort(key = lambda x : x.priority, reverse = True)
        self.playlist.sort(key = lambda x : x.start, reverse=False)
        # Now check for insertions.  Specials that overlap regular programming.
        self.fix_overlapped()


    def run_schedule(self, to_play):
        '''
        TODO:  Bug-278. When playlist end, re-sort and restart
        '''
        while(1):
            for item in to_play:
                #DEBUG("{}, {}, {}, {}, {}".format(item.start, item.end, datetime.now() >= item.start, datetime.now() <= item.end, item.URL))
                if datetime.now() >= item.start and datetime.now() < item.end:
                    #DEBUG("Playing {}".format(item.URL))
                    item.play()
                    while item.player.is_alive():
                        time.sleep(10)
                        for x in self.playlist:
                            now = datetime.now()
                            if now > x.start and now < x.end and x is not item:
                                DEBUG("Need to stop an play another thing!")
            time.sleep(10)
            self.get_playlist()


    def fix_overlapped(self):
        '''
        Find items where items overlap, the 4 possible cases are shown here.
        Case:
        1)  ------------------------------           <- o
                ----------------                     <- i

        2)       --------------------------             o
            ----------------                            i

        3)  -------------------------------             o
                               ----------------------   i

        4)              --------                        o
                 -------------------------              i

        5)          ------------                        o
                    ------------                        i
        In pseudo code:
        1)  S.start > R.start and S.start < R.end and S.end < R.end
        2)  S.start < R.start and S.end > R.start and S.end < R.end
        3)  S.start > R.start and S.start < R.end and S.end > R.end
        4)  S.start < R.start and S.end > R.end
        5)  End and start time match
        6)  Special programs have a defined start day/date and should always override regular progs

        But:
        2 includes 3
        3 includes 2
        4 includes 2 and 3 because S also overlaps items either side of R.

        Arguably this can be reduced to two cases each having mirror conditions.  An overlap and a
        completely inside.

        Special case 4 where start and and times are the same, in which case we promote the special

        Do we shorten earlier progs to the start time of later ones or vv?
        I think, specials that occur later should take priority over earlier overlapping specials, maybe.

        '''
        # For this method we are going to assume that items added lower in the list
            # are going to subvert those higher in the list, in the assumption that regular programming
            # will at the top and not change often, lower items will be additions, amendments etc.
        playlen = len(self.playlist)
        #DEBUG(f"playlen: {playlen}")
        for o in range(playlen):
            #DEBUG(f"o:{o}")
            if self.playlist[o].start.date() < datetime.now().date(): continue
            for i in range(o+1, playlen):
                #DEBUG(f"i:{i}")
                #DEBUG(f"playlen: {playlen}")
                if i >= playlen or o >= playlen: # Playlist has been truncated? # Bug-324
                    return
                if not isinstance(self.playlist[i].start, datetime): continue # ie, If it's a comment line.
                if self.playlist[i].start > self.playlist[o].start \
                        and self.playlist[i].end < self.playlist[o].end: # i lies entirely within o
                    if not self.playlist[o].special: # Specials do not get edited
                        # Split o to let i interrupt it
                        DEBUG("Fixing case 1")
                        o1 = copy.deepcopy(self.playlist[o]) # Need to split o and copy it
                        o2 = copy.deepcopy(self.playlist[o]) # Use copy else we get links instead!
                        self.playlist.remove(self.playlist[o]) # remove the original
                        playlen -= 1 # This gets undone later I know but it's here for integrity
                        # TODO Because this one is complicated! Also borken
                        o1.end = self.playlist[i].start
                        o2.start = self.playlist[i].end
                        # Where do we insert it and then what do we do?
                        self.playlist.append(o2)
                        self.playlist.append(o1)
                        playlen += 2

                elif self.playlist[o].start > self.playlist[i].start \
                        and self.playlist[o].end > self.playlist[i].end:
                    DEBUG("Fixing case 2")
                    self.playlist[o].start = self.playlist[i].end

                elif self.playlist[o].start < self.playlist[i].start \
                        and self.playlist[o].end > self.playlist[i].start:
                    # i starts before o ends
                    DEBUG("Fixing case 3")
                    self.playlist[o].end = self.playlist[i].start

                elif self.playlist[o].start > self.playlist[i].start \
                        and self.playlist[o].end < self.playlist[i].end:
                    # Just replace o with i or just remove it?
                    DEBUG("Fixing case 4")
                    self.playlist.remove(self.playlist[o])
                    playlen -= 1  # Fix for Bug-324 maybe?
                    #self.playlist[o] = self.playlist[i]

                elif self.playlist[o].start == self.playlist[i].start \
                        and self.playlist[o].end == self.playlist[i].end:
                    # Just remove it?
                    DEBUG("Fixing case 5")
                    INFO(f"Removing {self.playlist[o]}" )
                    self.playlist.remove(self.playlist[o])
                    playlen -= 1  # Fix for Bug-324 maybe?


        #self.playlist.sort(key = lambda x : x.start, reverse=False)
        self.save_to_csv()
        return


    def save_to_csv(self, fname = None):
        self.playlist.sort(key = lambda x : x.start, reverse=False)
        if fname == None:
            ofile = open(os.path.join(pwd,"sched_bak.csv"),"w")
        else:
            ofile = open(fname,"w")
        ofile.write("date_start, date_end, time_start, time_end, repeat, URL, description\n")
        for item in self.playlist:
            ofile.write(item.to_csv() + "\n")
        ofile.close()
        return

    def print_schedule(self):
        # Print the internal representation of the schedule then exit.
        self.playlist.sort(key = lambda x : x.start, reverse=False)
        for item in self.playlist:
            print(item.to_csv())
        quit(0)


    def exit_nicely(self, signal, frame):
        self.save_to_csv()
        quit(0)


    def main(self):
        app = Scheduler()


#################### END Scheduler #####################################



class GuiScheduler(Scheduler):
    '''
    A GUI version based on the above for inputting and viewing schedules
    '''
    def __init__(self):
        DEBUG("Welcome to the GUI Age")
        signal.signal(signal.SIGINT, self.exit_nicely)
        #self.check_instance()
        #self.pulse = pulsectl.Pulse('scheduler.py')
        self.playlist = [] # list of media to be played
        self.play_thread = None # Gets created to play the playlist
        self.playing = None # The currently item playing, we need to access it to stop it.
        self.play = False # Gets set when the play button is pressed and unset when stop is pressed.
        builder = gtk.Builder()
        self.gladefile = os.path.join(os.path.dirname(__file__),"ui/main.glade")
        builder.add_from_file(self.gladefile)
        builder.connect_signals(self)
        window = builder.get_object("main_window")
        self.layout = builder.get_object("layout")
        self.scroll_window = builder.get_object("scroll_window")
        self.scroll_window.set_overlay_scrolling(0)
        self.text_view_playing = builder.get_object("text_view_playing")
        self.calendar = builder.get_object("calendar")
        self.volume = builder.get_object("vol_slider")
        self.volume.set_value(0.4) # This is what we set mplayer volume to so...
        self.treeview = builder.get_object("treeview")
        self.treeview.connect("button_release_event", self.add_new_item)
        self.treeview_exists = False
        self.volume.value = 0.40
        today = datetime.today()
        self.calendar.mark_day(today.day)
        self.calendar.select_day(today.day)
        self.calendar.select_month(today.month - 1, today.year)
        self.get_playlist()
        self.create_display_list(self.playlist)
        window.show_all()
        gtk.main()
        return



    def create_display_list(self, playlist):
        # create a liststore
        self.liststore = gtk.ListStore(str, str,str, str)
        # add rows of data to the liststore
        for item in playlist:
            DEBUG("{}, {}, {}, {}".format(item.start,item.end,
                    item.start.strftime("%H:%M"),
                    item.end.strftime("%H:%M")))
            self.liststore.append([\
                item.start.strftime("%Y-%m-%d") if item.repeat != 2 else '', #TODO Bug-323
                item.start.strftime("%H:%M"),
                item.end.strftime("%H:%M"),
                item.URL])

        # create the TreeView using liststore
        self.treeview.set_model(self.liststore)
        self.treeview.set_grid_lines(gtk.TreeViewGridLines(1))
        self.treeview.set_headers_clickable(True)

        # create the TreeViewColumns to display the data
        date_start_col = gtk.TreeViewColumn("Day/Date")
        time_start_col = gtk.TreeViewColumn("Time Start")
        #date_end_col = gtk.TreeViewColumn(" Date End ")
        time_end_col = gtk.TreeViewColumn("Time End")
        URL_col = gtk.TreeViewColumn("URL")

        # add columns to treeview, but only if they don't already exist.
        if self.treeview_exists == False:
            self.treeview.append_column(date_start_col)
            self.treeview.append_column(time_start_col)
            self.treeview.append_column(time_end_col)
            self.treeview.append_column(URL_col)
            self.treeview_exists = True

        # create a CellRenderers to render the data and properties
        self.cell_date_start = gtk.CellRendererText()
        self.cell_date_start.set_property("editable", True)
        self.cell_time_start = gtk.CellRendererText()
        self.cell_time_start.set_property("editable", True)
        self.cell_time_end = gtk.CellRendererText()
        self.cell_time_end.set_property("editable", True)
        self.cell_URL = gtk.CellRendererText()
        self.cell_URL.set_property("editable", True)

        # set background color property
        self.cell_date_start.set_property('cell-background', 'yellow')
        self.cell_time_start.set_property('cell-background', '#90EE90')
        self.cell_time_end.set_property('cell-background', '#ADD8E6')

        # add the cells to the columns
        date_start_col.pack_start(self.cell_date_start, False)
        time_start_col.pack_start(self.cell_time_start, False)
        time_end_col.pack_start(self.cell_time_end, False)
        URL_col.pack_start(self.cell_URL, False)

        # Add the data
        date_start_col.set_attributes(self.cell_date_start, text = 0)
        time_start_col.set_attributes(self.cell_time_start, text = 1)
        time_end_col.set_attributes(self.cell_time_end, text = 2)
        URL_col.set_attributes(self.cell_URL, text = 3)

        # Any edits would require a re-sort of the columns
        self.cell_date_start.connect("edited", self.cell_date_start_edited)
        self.cell_time_start.connect("edited", self.cell_time_start_edited)
        self.cell_time_end.connect("edited", self.cell_time_end_edited)
        self.cell_URL.connect("edited", self.cell_URL_edited)

        # No but leave them here remind of that thing I have to do
        # make treeview searchable
        #self.treeview.set_search_column(0)
        # Allow sorting on the column
        # date_start_col.set_sort_column_id(0)
        # Allow drag and drop reordering of rows
        #self.treeview.set_reorderable(True)
        return



    def add_new_item(self, tv, event):
        '''
        Add new item to playlist by appending to self.liststore and updating the playlist
        This should pop-up and window asking what you want to do. ADD, DELETE or EDIT.
        '''
        return #TODO
        if event.button == 3:
            from datetime import date
            DEBUG("B3 pressed")
            # Update the playlist

            theday = self.calendar.get_date()
            DEBUG(theday.year)
            day = datetime.strftime(date(theday.year, theday.month + 1, theday.day), "%Y-%m-%d")
            time = datetime.strftime( datetime.now(), "%H:%M")
            self.playlist.append(Media(day, day, time,  time,"","URL EDIT ME!","DESCRIBE ME"))
            self.playlist.sort(key = lambda x : x.start, reverse=False)
            self.fix_overlapped()
            # Update the display
            self.create_display_list(self.playlist)



    def treeview_row_activated_cb(self, widget, path, col):
        return


    def cell_edited(self, widget, row_idx, text):
        DEBUG(row_idx)

    # Bug-320 Do something useful here
    def cell_date_start_edited(self, widget, path, text): # Start date
        index = self.treeview.get_selection().get_selected_rows()[1][0][0]
        self.liststore[path][0] = text # datetime.strptime(self.format_time(text), "%Y-%m-%d")
        self.playlist[index].set_start_date(text)
        self.create_display_list(self.playlist) # This will sort the list
        self.save_to_csv("edited_sched.csv")

    def cell_time_start_edited(self, widget, path, text): # Start time
        index = self.treeview.get_selection().get_selected_rows()[1][0][0]
        start = self.playlist[index].start
        self.playlist[index].set_start_time(text)
        self.liststore[path][1] = text
        self.save_to_csv("edited_sched.csv")

    def cell_time_end_edited(self, widget, path, text): # End time
        index = self.treeview.get_selection().get_selected_rows()[1][0][0]
        start = self.playlist[index].start
        self.playlist[index].set_end_time(text)
        self.liststore[path][2] = self.format_time(text)
        self.save_to_csv("edited_sched.csv")

    def cell_URL_edited(self, widget, path, text): # URL
        self.liststore[path][3] = text
        self.create_display_list(self.playlist) # This will sort the list
        self.save_to_csv("edited_sched.csv")


    def format_time(self, text):
        ''' Format input to XX:XX time format '''
        try:
            text = datetime.strftime(datetime.strptime(text, "%H%M"), "%H:%M")
        except:
            text = datetime.strftime(datetime.strptime(text, "%H:%M"), "%H:%M")
        return text


    def play_list_run(self, widget):
        '''
        Play button was pressed.  Do the play thing.
        '''
        if self.playing: return # Already playing!
        self.play = True
        self.play_thread = threading.Thread(target = self.play_list, args = ())
        self.play_thread.start()

    def play_list(self):
        '''
        Instanciate in a thread from play_list_run()
        '''
        if not self.play_thread: return
        while self.play:
            DEBUG("Playing some stuff...")
            self.playlist.sort(key=lambda x: x.start, reverse=False)
            # Find the playable item
            for item in self.playlist:
                DEBUG("{}, {}, {}, {}, {}".format(item.start, item.end, datetime.now() >= item.start, datetime.now() <= item.end, item.URL))
                if datetime.now() >= item.start and datetime.now() < item.end:
                    self.playing = item # Needs to be global variable
                    buf = self.text_view_playing.get_buffer()
                    track = item.get_streamtitle()
                    buf.set_text("{} : {}".format(item.URL,track))
            if not self.playing: # If there's no item to play? Wait for a bit then try again.
                time.sleep(5) # Not too long, this will matter when stopping the thread.
            elif self.playing:
                self.playing.play()
                while self.playing and self.play: # Wait until play ends
                    time.sleep(60)
                    if not self.playing.player.is_alive():
                        break
            if self.playing:
                self.playing.play_stop()
        if self.play:
            DEBUG("Trying next item")
            self.play_list() # If still in play mode, repeat


    def play_list_stop(self, widget):
        '''
        Stop button was pressed. Stop stuff
        '''
        if not self.playing: return # Already stopped
        self.play = False
        DEBUG("Trying to stop playing. This may take a literal minute. Be patient.")
        if self.playing:
            self.playing.play_stop()
        if self.play_thread:
            DEBUG("Trying to join thread")
            self.play_thread.join(2) # I can wait for 2 seconds only.
        DEBUG("Stopped")
        buf = self.text_view_playing.get_buffer()
        buf.set_text("")
        self.playing = None


    def gtk_main_quit(self, widget):
        # Stop any playing media before quitting
        self.play_list_stop(None)
        if self.play_thread:  self.play_thread.join()
        self.save_to_csv()
        gtk.main_quit()
        quit(0)


    def on_volume_changed(self, widget, value):
        DEBUG(value)
        try:
            for sink in self.pulse.sink_list():
                self.pulse.volume_set_all_chans(sink, value)
        except AttributeError as ae:
            print(ae)
            pass
        return


    def set_volume(self, volume):
        ''' Set output volume.
        This may be BT device or internal sound card.  Set them all.
        '''
        for sink in pulse.sink_list():
            self.pulse.volume_set_all_chans(sink, volume)
        return

#################### END GuiScheduler #####################################


if __name__ == "__main__":
    args = parser.parse_args()
    if args.gui:
        app = GuiScheduler()

    else:
        print("Running without a GUI")
        app = Scheduler()
