= Media Playout Scheduler

This takes a play schedule from a CSV file and plays the items at the scheduled time.
Why?  Because I like to vary my entertainment by time of day or day of week.
Using this means I can get on with my work and not be bored/angry by what's playing or miss special programmes.
Obviously this is not perfect. It also works as an alarm clock.

I also use a small, legal, FM transmitter to broadcast the output to all the radios in the house, or via bluetooth to a device.
This way I get synchronised entertainment wherever I am without having to re-tune radios by time of day.

It is possible to make a playlist for each day of the week with daily and weekly repeats.  This is ongoing.

Why not a personal entertainment device I hear you ask?  Just, no.

A sample playlist is included.  Open it with your favourite spreadsheet application, edit it and re-save as a CSV formatted file.  Dates are in *proper ISO8601 format* obviously, this is the 21st century after all.
Times should be able ro be input without the colon to make typing easier.
I could do this date too I guess.  See Bug-275.
Leave out the date parts for daily repeats.

Use the repeat column to schedule repeats:
0 = No repeat, hourly = 1, daily = 2, weekly = 3, SPARE = 4
Not all of this has be programmed yet.

If no date supplied then a daily repeat is assumed at the moment.
If a day of the week is specified then it's a weekly repeat.
I guess these values are no longer required in the schedule file as the repeats can be inferred from the supplied data. TODO.

Currently the play list is saved on exit to "sched_bak.csv" file as backup.

image::doc/app.png[]

== Using it

Install the requirements with:

[script,bash]
pip3 install -r requirements.txt

Install with:

[script,bash]
pip3 install . --user

Unless you want it installed as root of course.

Use it;  "./scheduler.py -h" will give the usage.

Don't rely on this to program a real radio station.  Other software are available.

== Bugs

Use the link in GitLab.

Don't bug about not using VLC, I could have but I didn't, deal with it.
